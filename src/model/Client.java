package model;

public class Client extends Entity{
	
	private Account account;
	private String name;
	private int cardId;
	private int CNP;
	private String address;
	
	public Client(){}
	
	public Client(String name, int cardId, int CNP, String address){
		this.name = name;
		this.cardId = cardId;
		this.CNP = CNP;
		this.address = address;
	}
	

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public int getCNP() {
		return CNP;
	}

	public void setCNP(int cNP) {
		CNP = cNP;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	

}
