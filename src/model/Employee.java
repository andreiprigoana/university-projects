package model;

import java.sql.Date;

public class Employee extends Entity{

	private int employeeId;
	private String name;
	private int CNP;
	private String address;
	private Date date;
	
	public Employee(){}
	
	public Employee(int employeeId, String name, int CNP, String address, Date date){
		this.employeeId = employeeId;
		this.name = name;
		this.CNP = CNP;
		this.address = address;
		this.date = date;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCNP() {
		return CNP;
	}

	public void setCNP(int cNP) {
		CNP = cNP;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
