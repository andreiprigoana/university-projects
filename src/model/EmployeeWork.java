package model;

import java.sql.Date;

public class EmployeeWork extends Entity{
	

	private int employeeId;
	private String name;
	private Date date;
	private String operation;
	
	public EmployeeWork(){}
	
	public EmployeeWork(int employeeId, String name, Date date, String operation){
		this.employeeId = employeeId;
		this.name = name;
		this.date = date;
		this.operation = operation;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
}
