package model;

public class User extends Entity{
	
	private String userName;
	private String password;
	private String type;
	private int employeeId;
	
	public User(){}
	
	public User(String userName, String password, String type, int employeeId){
		this.userName = userName;
		this.password = password;
		this.type = type;
		this.employeeId = employeeId;
	}
	
	public User(String password, int employeeId){
		this.password = password;
		this.employeeId = employeeId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
}
