package model;

import java.sql.Date;

public class Account extends Entity {

	private int accountId;
	private String type;
	private int CNP;
	private int money;
	private Date date;

	public Account() {
	}

	public Account(int accountId, String type, int CNP, int money, Date date) {
		this.accountId = accountId;
		this.type = type;
		this.CNP = CNP;
		this.money = money;
		this.date = date;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCNP() {
		return CNP;
	}

	public void setCNP(int CNP) {
		this.CNP = CNP;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
