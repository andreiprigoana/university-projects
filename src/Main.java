import model.Client;
import presentation.Controller;
import presentation.View;
import presentation.ViewPassword;
import presentation.ViewReport;
import presentation.ViewTransfer;

public class Main {

	public static void main(String[] args){
		
		View view = new View();
		view.getFrame().setVisible(true);
		Controller c = new Controller(view);		
		
		view.populateClientsTable();
		view.populateAccountTable();
		view.populateEmployeeTable();
		
			
	}
}