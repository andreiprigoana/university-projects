package presentation;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Date;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import dataAccesLayer.DBConnection;

public class View {
	
	private DBConnection connect;
	private Controller controller;
	
	JFrame frame;
	
	private JLabel label1;
	private JLabel label2;
	
	private JPanel p1;
	private JPanel p2;
	private JPanel p3;
	private JPanel p4;
	private JPanel p5;
	
	private JTabbedPane tab1;
	
	private DefaultTableModel model1;
	private DefaultTableModel model2;
	private DefaultTableModel model3;
	
	private JTable t1;
	private JTable t2;
	private JTable t3;
	
	private JScrollPane scroll1;
	private JScrollPane scroll2;
	private JScrollPane scroll3;
	
	private JButton b1;
	private JButton b2;
	private JButton b3;
	private JButton b4;
	private JButton b5;
	private JButton b6;
	private JButton b7;
	private JButton b8;
	private JButton b9;
	private JButton b10;
	private JButton b11;
	private JButton b12;
	private JButton b13;
	private JButton b14;
	private JButton b15;
	private JButton b16;
	private JButton b17;
	private JButton b18;
	private JButton b19;
	private JButton b20;
	private JButton b21;
	private JButton b22;
	
	private JTextField f1;
	private JTextField f2;
	private JTextField f3;
	private JTextField f4;
	private JTextField f5;
	private JTextField f6;
	private JTextField f7;
	private JTextField f8;
	private JTextField f9;
	private JTextField f10;
	private JTextField f11;
	private JTextField f12;
	private JTextField f13;
	private JTextField f14;
	private JTextField f15;
	private JTextField f16;
	private JTextField f17;
	
	private JLabel l1;
	private JLabel l2;
	private JLabel l3;
	private JLabel l4;
	private JLabel l5;
	private JLabel l6;
	private JLabel l7;
	private JLabel l8;
	private JLabel l9;
	private JLabel l10;
	private JLabel l11;
	private JLabel l12;
	private JLabel l13;
	private JLabel l14;
	private JLabel l15;

	
	public View(){
		
		frame = new JFrame("Bank Manager");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		p1 = new JPanel();
		frame.add(p1);
		
		p1 = new JPanel();
		p2 = new JPanel();
		p3 = new JPanel();
		p4 = new JPanel();
		p5 = new JPanel();
		
		p1.setLayout( new BorderLayout() );
		p2.setLayout( new BorderLayout() );
		p3.setLayout( new BorderLayout() );
		p4.setLayout( new BorderLayout() );
		p5.setLayout( new BorderLayout() );
		
		tab1 = new JTabbedPane();
		
		b1 = new JButton("Add Client");
		b2 = new JButton("Update Client");
		b3 = new JButton("View Client");
		b4 = new JButton("View All");
		b5 = new JButton("Add Account");
		b6 = new JButton("View Account");
		b7 = new JButton("Update Account");
		b8 = new JButton("Delete Account");
		b9 = new JButton("Transfer Money");
		b10 = new JButton("Process Bills");
		b11 = new JButton("Add Employee");
		b12 = new JButton("View Employee");
		b13 = new JButton("Update Employee");
		b14 = new JButton("Delete Employee");
		b15 = new JButton("Work Report");
		b16 = new JButton("View All");
		b17 = new JButton("Log Out");         //Employee
		b18 = new JButton("Change Password"); //Employee
		b19 = new JButton("Log Out");         //Administrator
		b20 = new JButton("Change Password"); //Administrator
		b21 = new JButton("Login");           //Employee
		b22 = new JButton("Login");           //Administrator
		
		
		f1 = new JTextField();   //Client CardNumber
		f2 = new JTextField();   //Client Name
		f3 = new JTextField();   //Client CNP
		f4 = new JTextField();   //Client Address
		f5 = new JTextField();   //Account ID
		f6 = new JTextField();   //Account Type
		f7 = new JTextField();   //Account Money
		f8 = new JTextField();   //Employee ID
		f9 = new JTextField();   //Employee Name
		f10 = new JTextField();  //Employee CNP
		f11 = new JTextField();  //Employee Address
		f12 = new JTextField();  //Employee User
		f13 = new JTextField();  //Employee Password
		f14 = new JTextField();  //Administrator User
		f15 = new JTextField();  //Administrator Password
		
		l1 = new JLabel("Card Number");
		l2 = new JLabel("       Name");
		l3 = new JLabel("        CNP");
		l4 = new JLabel("    Address");
		l5 = new JLabel(" Account ID");
		l6 = new JLabel("       Type");
		l7 = new JLabel("      Money");
		
		l8 = new JLabel("Date");
		
		l9 = new JLabel("Employee ID");
		l10 = new JLabel("      Name");
		l11 = new JLabel("       CNP");
		l12 = new JLabel("   Address");
		
		l13 = new JLabel("Enter the username and the password.");
		l14 = new JLabel("Enter the username and the password.");
		l15 = new JLabel("");
		
		label1 = new JLabel();
		label1.setIcon(new ImageIcon("C:\\Users\\A\\Desktop\\bank2.jpg"));
		p1.add(label1);
		label2 = new JLabel();
		label2.setIcon(new ImageIcon("C:\\Users\\A\\Desktop\\bank2.jpg"));
		p2.add(label2);
		
		tab1.add("EMPLOYEE", p1);
        tab1.add("ADMINISTRATOR", p2);
        
        frame.add(tab1);
		
		// ------------------------------------------- TABELE ------------------------------------------- //
				
		model1 = new DefaultTableModel(); // TABLE FOR ADMINISTRATOR
		model1.addColumn("ID");
		model1.addColumn("Name");
		model1.addColumn("CNP");
		model1.addColumn("Address");
		model1.addColumn("Date");
		t1 = new JTable(model1);
		p3.setBounds(860, 80, 390, 520);
		scroll1 = new JScrollPane(t1);
		p3.add(scroll1);
		p2.add(p3);
		
		model2 = new DefaultTableModel(); // TABEL1 FOR EMPLOYEE(CLIENTS) DREAPTA
		model2.addColumn("ID");
		model2.addColumn("TYPE");
		model2.addColumn("MONEY");
		model2.addColumn("DATE");
		t2 = new JTable(model2);
		p4.setBounds(910, 30, 400, 500);
		scroll2 = new JScrollPane(t2);
		p4.add(scroll2);
		p1.add(p4);
	
		model3 = new DefaultTableModel(); // TABEL2 FOR EMPLOYEE(ACCOUNTS) STANGA
		model3.addColumn("NAME");
		model3.addColumn("CARD NUMBER");
		model3.addColumn("CNP");
		model3.addColumn("ADDRESS");
		t3 = new JTable(model3);
		p5.setBounds(40, 30, 400, 500);
		scroll3 = new JScrollPane(t3);
		p5.add(scroll3);
		p1.add(p5);
				
		// ------------------------------------------------------------------------------------------------- //	
		
		b1.setBounds(170, 550, 160, 20);
		b2.setBounds(170, 580, 160, 20);
		b3.setBounds(170, 610, 160, 20);
		b4.setBounds(170, 640, 160, 20);
		b5.setBounds(1030, 550, 160, 20);
		b6.setBounds(1030, 580, 160, 20);
		b7.setBounds(1030, 610, 160, 20);
		b8.setBounds(1030, 640, 160, 20);
		b9.setBounds(510, 570, 160, 30);
		b10.setBounds(690, 570, 160, 30);
		
		b11.setBounds(150, 500, 160, 30);
		b12.setBounds(350, 500, 160, 30);
		b13.setBounds(550, 500, 160, 30);
		b14.setBounds(150, 550, 160, 30);
		b15.setBounds(350, 550, 160, 30);
		b16.setBounds(550, 550, 160, 30);
		
		b17.setBounds(510, 620, 160, 30);
		b17.setForeground(Color.RED);
		b18.setBounds(690, 620, 160, 30);
		b18.setForeground(Color.RED);
		
		b19.setBounds(10, 10, 160, 30);
		b19.setForeground(Color.RED);
		b20.setBounds(10, 50, 160, 30);
		b20.setForeground(Color.RED);
		
		b21.setBounds(600, 430, 160, 30);
		b22.setBounds(600, 430, 160, 30);
		
		p1.add(b1);
		p1.add(b2); 
		p1.add(b3);
		p1.add(b4);
		p1.add(b5);
		p1.add(b6);
		p1.add(b7);
		p1.add(b8);
		p1.add(b9);
		p1.add(b10);
		p2.add(b11);
		p2.add(b12);
		p2.add(b13);
		p2.add(b14);
		p2.add(b15);
		p2.add(b16);
		p1.add(b17);
		p1.add(b18); //LOG OUT EMPLOYEE
		p2.add(b19); //CHANGE PASSWORD EMPLOYEE
		p2.add(b20);
		p1.add(b21);
		p2.add(b22);
		
		l1.setBounds(635, 20, 160, 30);
		l1.setForeground(Color.BLUE);
		l2.setBounds(635, 90, 160, 30);
		l2.setForeground(Color.BLUE);
		l3.setBounds(635, 160, 160, 30);
		l3.setForeground(Color.BLUE);
		l4.setBounds(635, 230, 160, 30);
		l4.setForeground(Color.BLUE);
		l5.setBounds(635, 300, 160, 30);
		l5.setForeground(Color.BLUE);
		l6.setBounds(635, 370, 160, 30);
		l6.setForeground(Color.BLUE);
		l7.setBounds(635, 440, 160, 30);
		l7.setForeground(Color.BLUE);
	
		l9.setBounds(200, 80, 160, 30);
		l9.setForeground(Color.black);
		l10.setBounds(200, 180, 160, 30);
		l10.setForeground(Color.black);
		l11.setBounds(200, 280, 160, 30);
		l11.setForeground(Color.black);
		l12.setBounds(200, 380, 160, 30);
		l12.setForeground(Color.black);
		
		l13.setBounds(350, 130, 650, 60);
		l13.setFont(new Font("Times New Roman", Font.ITALIC, 40));
		l13.setForeground(Color.YELLOW);
		l14.setBounds(350, 130, 650, 60);
		l14.setFont(new Font("Times New Roman", Font.ITALIC, 40));
		l14.setForeground(Color.YELLOW);
		
		p1.add(l1);
		p1.add(l2);
		p1.add(l3);
		p1.add(l4);
		p1.add(l5);
		p1.add(l6);
		p1.add(l7);
		p2.add(l9);
		p2.add(l10);
		p2.add(l11);
		p2.add(l12);
		p1.add(l13);
		p2.add(l14);
		
		f1.setBounds(550, 50, 250, 25);
		f2.setBounds(550, 120, 250, 25);
		f3.setBounds(550, 190, 250, 25);
		f4.setBounds(550, 260, 250, 25);
		f5.setBounds(550, 330, 250, 25);
		f6.setBounds(550, 400, 250, 25);
		f7.setBounds(550, 470, 250, 25);
		f8.setBounds(300, 80, 400, 25);
		f9.setBounds(300, 180, 400, 25);
		f10.setBounds(300, 280, 400, 25);
		f11.setBounds(300, 380, 400, 25);
		f12.setBounds(480, 280, 400, 25);
		f13.setBounds(480, 320, 400, 25);
		f14.setBounds(480, 280, 400, 25);
		f15.setBounds(480, 320, 400, 25);
		
		p1.add(f1);
		p1.add(f2);
		p1.add(f3);
		p1.add(f4);
		p1.add(f5);
		p1.add(f6);
		p1.add(f7);
		p2.add(f8);
		p2.add(f9);
		p2.add(f10);
		p2.add(f11);
		p1.add(f12);
		p1.add(f13);
		p2.add(f14);
		p2.add(f15);
		
		p1.add(label1);
		p2.add(label2);
		
		cleanP1();
		cleanP2();
	}
	
	public void cleanP1(){
		b1.setVisible(false);
		b2.setVisible(false);
		b3.setVisible(false);
		b4.setVisible(false);
		b5.setVisible(false);
		b6.setVisible(false);
		b7.setVisible(false);
		b8.setVisible(false);
		b9.setVisible(false);
		b10.setVisible(false);
		b17.setVisible(false);
		b18.setVisible(false);
		
		p4.setVisible(false);
		p5.setVisible(false);
		
		l1.setVisible(false);
		l2.setVisible(false);
		l3.setVisible(false);
		l4.setVisible(false);
		l5.setVisible(false);
		l6.setVisible(false);
		l7.setVisible(false);
		
		f1.setVisible(false);
		f2.setVisible(false);
		f3.setVisible(false);
		f4.setVisible(false);
		f5.setVisible(false);
		f6.setVisible(false);
		f7.setVisible(false);
	}
	
	public void cleanFirstP1(){
		b21.setVisible(false);
		
		f12.setVisible(false);
		f13.setVisible(false);
		
		l13.setVisible(false);
	}
	
	public void apearFirstP1(){
		b21.setVisible(true);
		
		f12.setVisible(true);
		f13.setVisible(true);
		
		l13.setVisible(true);
	}
	
	public void apearP1(){
		b1.setVisible(true);
		b2.setVisible(true);
		b3.setVisible(true);
		b4.setVisible(true);
		b5.setVisible(true);
		b6.setVisible(true);
		b7.setVisible(true);
		b8.setVisible(true);
		b9.setVisible(true);
		b10.setVisible(true);
		b17.setVisible(true);
		b18.setVisible(true);
		
		p4.setVisible(true);
		p5.setVisible(true);
		
		l1.setVisible(true);
		l2.setVisible(true);
		l3.setVisible(true);
		l4.setVisible(true);
		l5.setVisible(true);
		l6.setVisible(true);
		l7.setVisible(true);
		
		f1.setVisible(true);
		f2.setVisible(true);
		f3.setVisible(true);
		f4.setVisible(true);
		f5.setVisible(true);
		f6.setVisible(true);
		f7.setVisible(true);
	}
	
	public void cleanP2(){
		b11.setVisible(false);
		b12.setVisible(false);
		b13.setVisible(false);
		b14.setVisible(false);
		b15.setVisible(false);
		b16.setVisible(false);
		b19.setVisible(false);
		b20.setVisible(false);
		
		l9.setVisible(false);
		l10.setVisible(false);
		l11.setVisible(false);
		l12.setVisible(false);
		
		f8.setVisible(false);
		f9.setVisible(false);
		f10.setVisible(false);
		f11.setVisible(false);
		
		p3.setVisible(false);
	}
	
	public void cleanFirstP2(){
		b22.setVisible(false);
		
		f14.setVisible(false);
		f15.setVisible(false);
		
		l14.setVisible(false);
	}
	
	public void apearFirstP2(){
		b22.setVisible(true);
		
		f14.setVisible(true);
		f15.setVisible(true);
		
		l14.setVisible(true);
	}
	
	public void apearP2(){
		b11.setVisible(true);
		b12.setVisible(true);
		b13.setVisible(true);
		b14.setVisible(true);
		b15.setVisible(true);
		b16.setVisible(true);
		b19.setVisible(true);
		b20.setVisible(true);
		
		l9.setVisible(true);
		l10.setVisible(true);
		l11.setVisible(true);
		l12.setVisible(true);
		
		f8.setVisible(true);
		f9.setVisible(true);
		f10.setVisible(true);
		f11.setVisible(true);
		
		p3.setVisible(true);
	}
	
	public void addItemListener(ActionListener a) {
		b1.addActionListener(a);
		b2.addActionListener(a);
		b3.addActionListener(a);
		b4.addActionListener(a);
		b5.addActionListener(a);
		b6.addActionListener(a);
		b7.addActionListener(a);
		b8.addActionListener(a);
		b9.addActionListener(a);
		b10.addActionListener(a);
		b11.addActionListener(a);
		b12.addActionListener(a);
		b13.addActionListener(a);
		b14.addActionListener(a);
		b15.addActionListener(a);
		b16.addActionListener(a);
		b17.addActionListener(a);
		b18.addActionListener(a);
		b19.addActionListener(a);
		b20.addActionListener(a);
		b21.addActionListener(a);
		b22.addActionListener(a);
	}
	
	public void populateClientsTable(){
		connect = new DBConnection();
		try{
			String querry = "select * from client";
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				String name = connect.getRs().getString("name");
				int cardNR = connect.getRs().getInt("cardID");
				int CNP = connect.getRs().getInt("CNP");
				String address = connect.getRs().getString("Address");
				
				getModel3().addRow(new Object[] {name, cardNR, CNP, address});
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
	}
	
	public void populateClients(){
		connect = new DBConnection();
		try{
			String querry = "SELECT * FROM client WHERE CNP="+f3.getText();
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				String name = connect.getRs().getString("name");
				int cardNR = connect.getRs().getInt("cardID");
				int CNP = connect.getRs().getInt("CNP");
				String address = connect.getRs().getString("Address");
				
				getModel3().addRow(new Object[] {name, cardNR, CNP, address});
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
	}
	
	public void populateAccountTable(){
		connect = new DBConnection();
		try{
			String querry = "SELECT * FROM account";
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				String id = connect.getRs().getString("accountID");
				String type = connect.getRs().getString("type");
				int money = connect.getRs().getInt("money");
				Date date = connect.getRs().getDate("date");
				
				getModel2().addRow(new Object[] {id, type, money, date});
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
	}
	
	
	public void populateAccount(){
		connect = new DBConnection();
		try{
			String querry = "SELECT * FROM account WHERE accountID="+f5.getText();
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				String id = connect.getRs().getString("accountID");
				String type = connect.getRs().getString("type");
				int money = connect.getRs().getInt("money");
				Date date = connect.getRs().getDate("date");
				
				getModel2().addRow(new Object[] {id, type, money, date});
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
	}
	
	
	public void populateEmployeeTable(){
		connect = new DBConnection();
		try{
			String querry = "select * from employee";
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				int id = connect.getRs().getInt("employeeID");
				String name = connect.getRs().getString("name");
				int cnp = connect.getRs().getInt("CNP");
				String address = connect.getRs().getString("Address");
				Date date = connect.getRs().getDate("date");
				
				getModel1().addRow(new Object[] {id, name, cnp, address, date});
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
	}
	
	public void populateEmployee(){
		connect = new DBConnection();
		controller = new Controller();
		try{
			String querry = "SELECT * FROM employee WHERE employeeID="+f8.getText();
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				int id = connect.getRs().getInt("employeeID");
				String name = connect.getRs().getString("name");
				int cnp = connect.getRs().getInt("CNP");
				String address = connect.getRs().getString("Address");
				Date date = connect.getRs().getDate("date");
				
				getModel1().addRow(new Object[] {id, name, cnp, address, date});
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
	}

	
	public boolean checkClient(){
		connect = new DBConnection();
		
		try{
			String querry = "SELECT CNP FROM client WHERE CNP="+f3.getText();
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				String checkCNP = connect.getRs().getString("CNP");	
				if(checkCNP.equals(f3.getText())){
					return true;
				}
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
		return false;
	}
	
	public JButton getB21() {
		return b21;
	}

	public void setB21(JButton b21) {
		this.b21 = b21;
	}

	public JButton getB22() {
		return b22;
	}

	public void setB22(JButton b22) {
		this.b22 = b22;
	}

	public JButton getB17() {
		return b17;
	}

	public void setB17(JButton b17) {
		this.b17 = b17;
	}

	public JButton getB18() {
		return b18;
	}

	public void setB18(JButton b18) {
		this.b18 = b18;
	}

	public JButton getB19() {
		return b19;
	}

	public void setB19(JButton b19) {
		this.b19 = b19;
	}

	public JButton getB20() {
		return b20;
	}

	public void setB20(JButton b20) {
		this.b20 = b20;
	}

	public JTextField getF16() {
		return f16;
	}

	public void setF16(JTextField f16) {
		this.f16 = f16;
	}

	public JTextField getF17() {
		return f17;
	}

	public void setF17(JTextField f17) {
		this.f17 = f17;
	}

	public JFrame getFrame() {
		return frame;
	}


	public void setFrame(JFrame frame) {
		this.frame = frame;
	}


	public JLabel getLabel1() {
		return label1;
	}


	public void setLabel1(JLabel label1) {
		this.label1 = label1;
	}


	public JLabel getLabel2() {
		return label2;
	}


	public void setLabel2(JLabel label2) {
		this.label2 = label2;
	}


	public JPanel getP1() {
		return p1;
	}


	public void setP1(JPanel p1) {
		this.p1 = p1;
	}


	public JPanel getP2() {
		return p2;
	}


	public void setP2(JPanel p2) {
		this.p2 = p2;
	}


	public JPanel getP3() {
		return p3;
	}


	public void setP3(JPanel p3) {
		this.p3 = p3;
	}


	public JPanel getP4() {
		return p4;
	}


	public void setP4(JPanel p4) {
		this.p4 = p4;
	}


	public JPanel getP5() {
		return p5;
	}


	public void setP5(JPanel p5) {
		this.p5 = p5;
	}


	public JTabbedPane getTab1() {
		return tab1;
	}


	public void setTab1(JTabbedPane tab1) {
		this.tab1 = tab1;
	}


	public DefaultTableModel getModel1() {
		return model1;
	}


	public void setModel1(DefaultTableModel model1) {
		this.model1 = model1;
	}


	public DefaultTableModel getModel2() {
		return model2;
	}


	public void setModel2(DefaultTableModel model2) {
		this.model2 = model2;
	}


	public DefaultTableModel getModel3() {
		return model3;
	}


	public void setModel3(DefaultTableModel model3) {
		this.model3 = model3;
	}


	public JTable getT1() {
		return t1;
	}


	public void setT1(JTable t1) {
		this.t1 = t1;
	}


	public JTable getT2() {
		return t2;
	}


	public void setT2(JTable t2) {
		this.t2 = t2;
	}


	public JTable getT3() {
		return t3;
	}


	public void setT3(JTable t3) {
		this.t3 = t3;
	}


	public JScrollPane getScroll1() {
		return scroll1;
	}


	public void setScroll1(JScrollPane scroll1) {
		this.scroll1 = scroll1;
	}


	public JScrollPane getScroll2() {
		return scroll2;
	}


	public void setScroll2(JScrollPane scroll2) {
		this.scroll2 = scroll2;
	}


	public JScrollPane getScroll3() {
		return scroll3;
	}


	public void setScroll3(JScrollPane scroll3) {
		this.scroll3 = scroll3;
	}


	public JButton getB1() {
		return b1;
	}


	public void setB1(JButton b1) {
		this.b1 = b1;
	}


	public JButton getB2() {
		return b2;
	}


	public void setB2(JButton b2) {
		this.b2 = b2;
	}


	public JButton getB3() {
		return b3;
	}


	public void setB3(JButton b3) {
		this.b3 = b3;
	}


	public JButton getB4() {
		return b4;
	}


	public void setB4(JButton b4) {
		this.b4 = b4;
	}


	public JButton getB5() {
		return b5;
	}


	public void setB5(JButton b5) {
		this.b5 = b5;
	}


	public JButton getB6() {
		return b6;
	}


	public void setB6(JButton b6) {
		this.b6 = b6;
	}


	public JButton getB7() {
		return b7;
	}


	public void setB7(JButton b7) {
		this.b7 = b7;
	}


	public JButton getB8() {
		return b8;
	}


	public void setB8(JButton b8) {
		this.b8 = b8;
	}


	public JButton getB9() {
		return b9;
	}


	public void setB9(JButton b9) {
		this.b9 = b9;
	}


	public JButton getB10() {
		return b10;
	}


	public void setB10(JButton b10) {
		this.b10 = b10;
	}


	public JButton getB11() {
		return b11;
	}


	public void setB11(JButton b11) {
		this.b11 = b11;
	}


	public JButton getB12() {
		return b12;
	}


	public void setB12(JButton b12) {
		this.b12 = b12;
	}


	public JButton getB13() {
		return b13;
	}


	public void setB13(JButton b13) {
		this.b13 = b13;
	}


	public JButton getB14() {
		return b14;
	}


	public void setB14(JButton b14) {
		this.b14 = b14;
	}


	public JButton getB15() {
		return b15;
	}


	public void setB15(JButton b15) {
		this.b15 = b15;
	}


	public JButton getB16() {
		return b16;
	}


	public void setB16(JButton b16) {
		this.b16 = b16;
	}


	public JTextField getF1() {
		return f1;
	}

	public void setF1(JTextField f1) {
		this.f1 = f1;
	}

	public JTextField getF2() {
		return f2;
	}

	public void setF2(JTextField f2) {
		this.f2 = f2;
	}

	public JTextField getF3() {
		return f3;
	}

	public void setF3(JTextField f3) {
		this.f3 = f3;
	}

	public JTextField getF4() {
		return f4;
	}

	public void setF4(JTextField f4) {
		this.f4 = f4;
	}

	public JTextField getF5() {
		return f5;
	}

	public void setF5(JTextField f5) {
		this.f5 = f5;
	}

	public JTextField getF6() {
		return f6;
	}

	public void setF6(JTextField f6) {
		this.f6 = f6;
	}

	public JTextField getF7() {
		return f7;
	}

	public void setF7(JTextField f7) {
		this.f7 = f7;
	}

	public JTextField getF8() {
		return f8;
	}

	public void setF8(JTextField f8) {
		this.f8 = f8;
	}

	public JTextField getF9() {
		return f9;
	}

	public void setF9(JTextField f9) {
		this.f9 = f9;
	}

	public JTextField getF10() {
		return f10;
	}

	public void setF10(JTextField f10) {
		this.f10 = f10;
	}

	public JTextField getF11() {
		return f11;
	}

	public void setF11(JTextField f11) {
		this.f11 = f11;
	}

	public JTextField getF12() {
		return f12;
	}

	public void setF12(JTextField f12) {
		this.f12 = f12;
	}

	public JTextField getF13() {
		return f13;
	}

	public void setF13(JTextField f13) {
		this.f13 = f13;
	}

	public JTextField getF14() {
		return f14;
	}

	public void setF14(JTextField f14) {
		this.f14 = f14;
	}

	public JTextField getF15() {
		return f15;
	}

	public void setF15(JTextField f15) {
		this.f15 = f15;
	}

	public JLabel getL1() {
		return l1;
	}

	public void setL1(JLabel l1) {
		this.l1 = l1;
	}

	public JLabel getL2() {
		return l2;
	}

	public void setL2(JLabel l2) {
		this.l2 = l2;
	}

	public JLabel getL3() {
		return l3;
	}

	public void setL3(JLabel l3) {
		this.l3 = l3;
	}

	public JLabel getL4() {
		return l4;
	}

	public void setL4(JLabel l4) {
		this.l4 = l4;
	}

	public JLabel getL5() {
		return l5;
	}

	public void setL5(JLabel l5) {
		this.l5 = l5;
	}

	public JLabel getL6() {
		return l6;
	}

	public void setL6(JLabel l6) {
		this.l6 = l6;
	}

	public JLabel getL7() {
		return l7;
	}

	public void setL7(JLabel l7) {
		this.l7 = l7;
	}

	public JLabel getL8() {
		return l8;
	}

	public void setL8(JLabel l8) {
		this.l8 = l8;
	}

	public JLabel getL9() {
		return l9;
	}

	public void setL9(JLabel l9) {
		this.l9 = l9;
	}

	public JLabel getL10() {
		return l10;
	}

	public void setL10(JLabel l10) {
		this.l10 = l10;
	}

	public JLabel getL11() {
		return l11;
	}

	public void setL11(JLabel l11) {
		this.l11 = l11;
	}

	public JLabel getL12() {
		return l12;
	}

	public void setL12(JLabel l12) {
		this.l12 = l12;
	}

	public JLabel getL13() {
		return l13;
	}

	public void setL13(JLabel l13) {
		this.l13 = l13;
	}

	public JLabel getL14() {
		return l14;
	}

	public void setL14(JLabel l14) {
		this.l14 = l14;
	}

	public JLabel getL15() {
		return l15;
	}

	public void setL15(JLabel l15) {
		this.l15 = l15;
	}
}