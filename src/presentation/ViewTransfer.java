package presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.*;

import business.Repository;
import dataAccesLayer.AccountGateway;
import dataAccesLayer.ClientGateway;
import dataAccesLayer.DBConnection;
import model.Account;
import model.Client;
import model.Employee;

public class ViewTransfer implements ActionListener{
	
	private View view;
	private ViewTransfer viewTransfer;
	
	private Client client;
	private Employee employee;
	private Account account;
	private ClientGateway clientGateway;
	private AccountGateway accountGateway;
	
	private Repository repository;
	private DBConnection connect;
	
	private JFrame frame;
	
	private JPanel p1;
	
	private JLabel label1;
	
	private JButton b1;
	
	private JTextField f1;
	private JTextField f2;
	private JTextField f3;

	public ViewTransfer(){
		
		frame = new JFrame("Money Transfer");
		frame.setBounds(550, 200, 300, 200);
		
		p1 = new JPanel();
		frame.add(p1);
		
		label1 = new JLabel();
		
		p1.setLayout( new BorderLayout() );
		
		b1 = new JButton("Transfer");
		
		f1 = new JTextField(); // MONEY
		f2 = new JTextField(); // ACCOUNT ID 1
		f3 = new JTextField(); // ACCOUNT ID 2
		
		b1.setBounds(90, 110, 100, 20);
		p1.add(b1);
		
		f1.setBounds(90, 20, 100, 20);
		p1.add(f1);
		f2.setBounds(90, 50, 100, 20);
		p1.add(f2);
		f3.setBounds(90, 80, 100, 20);
		p1.add(f3);
		
		b1.addActionListener(this);
		
		p1.add(label1);

		frame.setVisible(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == b1){
			
			view = new View();
			accountGateway = new AccountGateway();
			
			if(f1.getText().equals("") || f2.getText().equals("") || f3.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else {
				accountGateway.transferMoney(Integer.parseInt(f2.getText()), Integer.parseInt(f3.getText()), Integer.parseInt(f1.getText()));
				
				int count = view.getModel2().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel2().removeRow(i);
				}
				frame.setVisible(false);
			}	
			view.populateAccountTable();
		}	
	}
	
	
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getP1() {
		return p1;
	}

	public void setP1(JPanel p1) {
		this.p1 = p1;
	}

	public JLabel getLabel1() {
		return label1;
	}

	public void setLabel1(JLabel label1) {
		this.label1 = label1;
	}

	public JButton getB1() {
		return b1;
	}

	public void setB1(JButton b1) {
		this.b1 = b1;
	}

	public JTextField getF1() {
		return f1;
	}

	public void setF1(JTextField f1) {
		this.f1 = f1;
	}

	public JTextField getF2() {
		return f2;
	}

	public void setF2(JTextField f2) {
		this.f2 = f2;
	}
}
