package presentation;

import java.awt.BorderLayout;
import java.sql.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dataAccesLayer.DBConnection;

public class ViewReport {
	

	private DBConnection connect;
	
	private JFrame frame;
	
	private JPanel p1;
	private JPanel p2;
	private JPanel p3;
	
	private DefaultTableModel model1;
	
	private JScrollPane scroll1;
	
	private JTable t1;
	
	private JLabel label1;
	private JLabel label2;
	
	public ViewReport(){
		
		frame = new JFrame("Work Report");
		frame.setBounds(350, 100, 700, 500);
		
		p1 = new JPanel();
		p2 = new JPanel();
		p3 = new JPanel();
		
		model1 = new DefaultTableModel();
		
		scroll1 = new JScrollPane();
		
		t1 = new JTable();
		
		frame.add(p1);

		
		
		label1 = new JLabel();
		label2 = new JLabel();
		
		p1.setLayout( new BorderLayout() );
		p2.setLayout( new BorderLayout() );
		p3.setLayout( new BorderLayout() );
		
		//-----------------------------------------------------------------------------------//
		model1 = new DefaultTableModel(); // TABLE FOR REPORT
		model1.addColumn("Employee ID");
		model1.addColumn("Name");
		model1.addColumn("Date");
		model1.addColumn("Operation");
		t1 = new JTable(model1);
		p3.setSize(frame.getSize());
		scroll1 = new JScrollPane(t1);
		p3.add(scroll1);
		p2.add(p3);
		
		frame.add(p2);
		//-----------------------------------------------------------------------------------//


		p2.add(label2);
		
		
		frame.setVisible(true);
	}
	
	public void populateWorkTable(String employeeId){
		int count = model1.getRowCount();
		for(int i = count-1 ; i>=0; i--){
			model1.removeRow(i);
		}
		
		connect = new DBConnection();
		try{
			String querry = "SELECT * FROM employeework where employeeID="+employeeId;
			connect.setRs(connect.getSt().executeQuery(querry));
			while(connect.getRs().next()){
				int id = connect.getRs().getInt("employeeID");
				String name = connect.getRs().getString("name");
				String operation = connect.getRs().getString("Operation");
				Date date = connect.getRs().getDate("Date");
				
				getModel1().addRow(new Object[] {id, name, date, operation});
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getP1() {
		return p1;
	}

	public void setP1(JPanel p1) {
		this.p1 = p1;
	}

	public JPanel getP2() {
		return p2;
	}

	public void setP2(JPanel p2) {
		this.p2 = p2;
	}

	public JPanel getP3() {
		return p3;
	}

	public void setP3(JPanel p3) {
		this.p3 = p3;
	}

	public DefaultTableModel getModel1() {
		return model1;
	}

	public void setModel1(DefaultTableModel model1) {
		this.model1 = model1;
	}

	public JScrollPane getScroll1() {
		return scroll1;
	}

	public void setScroll1(JScrollPane scroll1) {
		this.scroll1 = scroll1;
	}

	public JTable getT1() {
		return t1;
	}

	public void setT1(JTable t1) {
		this.t1 = t1;
	}

	public JLabel getLabel1() {
		return label1;
	}

	public void setLabel1(JLabel label1) {
		this.label1 = label1;
	}

	public JLabel getLabel2() {
		return label2;
	}

	public void setLabel2(JLabel label2) {
		this.label2 = label2;
	}
	
	
}
