package presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import business.Repository;
import model.Employee;
import model.User;

public class ViewPassword implements ActionListener{

	private View view;
	private Employee employee;
	private User user;
	private Repository repository;
	
	private JFrame frame;
	
	private JPanel p1;
	
	private JLabel label1;
	
	private JButton b1;
	
	private JTextField f1;
	private JTextField f2;
	private JTextField f3;

	public ViewPassword(){
		
		frame = new JFrame("Change Password");
		frame.setBounds(550, 200, 300, 200);
		
		p1 = new JPanel();
		frame.add(p1);
		
		label1 = new JLabel();
		
		p1.setLayout( new BorderLayout() );
		
		b1 = new JButton("Change");
		
		f1 = new JTextField(); // OLD PASSWORD
		f2 = new JTextField(); // NEW PASSWORD
		f3 = new JTextField(); // NEW PASSWORD
		
		b1.setBounds(90, 110, 100, 20);
		p1.add(b1);
		
		f1.setBounds(90, 20, 100, 20);
		p1.add(f1);
		f2.setBounds(90, 50, 100, 20);
		p1.add(f2);
		f3.setBounds(90, 80, 100, 20);
		p1.add(f3);
		
		p1.add(label1);

		b1.addActionListener(this);
		
		frame.setVisible(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		view = new View();
		
		if(e.getSource() == b1){
			if(f1.getText().equals("") || f2.getText().equals("") || f3.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else{
				if(f2.getText().equals(f3.getText())){
					user = new User(f2.getText(), Integer.parseInt(f1.getText()));
					frame.setVisible(false);
				} else if(f2.getText().equals(f3.getText())){
					JOptionPane.showMessageDialog(null, "Passwords are not the same!");
				}
				
			}
			
			repository = new Repository(user);

			repository.update(user);
		}
		
	}
	
	

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getP1() {
		return p1;
	}

	public void setP1(JPanel p1) {
		this.p1 = p1;
	}

	public JLabel getLabel1() {
		return label1;
	}

	public void setLabel1(JLabel label1) {
		this.label1 = label1;
	}

	public JButton getB1() {
		return b1;
	}

	public void setB1(JButton b1) {
		this.b1 = b1;
	}

	public JTextField getF1() {
		return f1;
	}

	public void setF1(JTextField f1) {
		this.f1 = f1;
	}

	public JTextField getF2() {
		return f2;
	}

	public void setF2(JTextField f2) {
		this.f2 = f2;
	}
}
