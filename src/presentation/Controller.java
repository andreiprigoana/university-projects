package presentation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JOptionPane;

import business.Repository;
import dataAccesLayer.ClientGateway;
import dataAccesLayer.DBConnection;
import dataAccesLayer.EmployeeGateway;
import dataAccesLayer.EmployeeWorkGateway;
import dataAccesLayer.GenericDataGateway;
import model.Account;
import model.Client;
import model.Employee;
import model.EmployeeWork;
import model.User;

public class Controller implements ActionListener{
	
	private static final String userName = "admin";
	private static final String password = "admin";
	
	private View view;
	private ViewTransfer viewTransfer;
	private ViewPassword viewPassword;
	private ViewReport viewReport;
	
	private Client client;
	private Employee employee;
	private EmployeeWork employeeWork;
	private Account account;
	private User user;
	private ClientGateway clientGateway;
	private EmployeeGateway employeeGateway;
	private EmployeeWorkGateway employeeWorkGateway;
	
	private Repository repository;
	private Repository repository1;
	private DBConnection connect;
	
	public Controller(){}
	
	public Controller(View view){
		this.view = view;
		this.view.addItemListener(this);
	}
	

	@Override
	public void actionPerformed(ActionEvent e) { // ADD CLIENT
		if(e.getSource() == view.getB1()){
			
			employeeGateway = new EmployeeGateway();
			
			if(view.getF2().getText().equals("") || view.getF1().getText().equals("") || view.getF3().getText().equals("") || view.getF4().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else{
				
				int count = view.getModel3().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel3().removeRow(i);
				}
				client = new Client(view.getF2().getText(), Integer.parseInt(view.getF1().getText()), Integer.parseInt(view.getF3().getText()), view.getF4().getText());
				employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Add Client "+view.getF3().getText());
				//public EmployeeWork(int employeeId, String name, Date date, String operation)
			}
			
			repository = new Repository(client);
			repository1 = new Repository(employeeWork);
			
			repository.create(client);
			repository1.create(employeeWork);
			
			view.populateClientsTable();
			
		} else if (e.getSource() == view.getB2()) { // UPDATE CLIENT
			
			employeeGateway = new EmployeeGateway();
			
			if(view.getF2().getText().equals("") || view.getF1().getText().equals("") || view.getF3().getText().equals("") || view.getF4().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else{
				
				int count = view.getModel3().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel3().removeRow(i);
				}
				client = new Client(view.getF2().getText(), Integer.parseInt(view.getF1().getText()), Integer.parseInt(view.getF3().getText()), view.getF4().getText());
				employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Update client "+view.getF3().getText());

			}
						
			repository = new Repository(client);
			repository1 = new Repository(employeeWork);

			repository.update(client);
			repository1.create(employeeWork);
			
			view.populateClientsTable();
			
			
		} else if (e.getSource() == view.getB3()) { // VIEW SPECIFIC CLIENT
			employeeGateway = new EmployeeGateway();
			
			if(view.getF3().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete client CNP!");
			} else{
				
				int count = view.getModel3().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel3().removeRow(i);
				}
				employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Viewed client "+view.getF3().getText());
				view.populateClients();	
			}
			
			repository1 = new Repository(employeeWork);
			
			repository1.create(employeeWork);
			
			
		} else if (e.getSource() == view.getB4()) { // VIEW ALL CLIENTS
			employeeGateway = new EmployeeGateway();
			
			
			
			int count = view.getModel3().getRowCount();
			for(int i = count-1 ; i>=0; i--){
				view.getModel3().removeRow(i);
			}
			employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Viewed all clients.");
			view.populateClientsTable();
			
			repository1 = new Repository(employeeWork);
			
			repository1.create(employeeWork);
			
		} else if (e.getSource() == view.getB5()) { // ADD ACCOUNT
			
			clientGateway = new ClientGateway();
			
			Calendar calendar = Calendar.getInstance();
     		java.util.Date currentDate = calendar.getTime();
			java.sql.Date date = new java.sql.Date(currentDate.getTime());
			
			if(view.getF5().getText().equals("") || view.getF6().getText().equals("") || view.getF3().getText().equals("") || view.getF7().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else {
				if(view.checkClient()){
					int count = view.getModel2().getRowCount();
					for(int i = count-1 ; i>=0; i--){
						view.getModel2().removeRow(i);
					}
					account = new Account(Integer.parseInt(view.getF5().getText()), view.getF6().getText(), Integer.parseInt(view.getF3().getText()), Integer.parseInt(view.getF7().getText()), date);
					employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), date, "Added account number ."+view.getF5().getText());
				} else {
					JOptionPane.showMessageDialog(null, "Client not found!");
				}
			}
					
			repository = new Repository(account);
			repository1 = new Repository(employeeWork);

			repository.create(account);
			repository1.create(employeeWork);
			
			view.populateAccountTable();
			
		} else if (e.getSource() == view.getB6()) { // VIEW SPECIFIC ACCOUNT
			
			if(view.getF5().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete account ID!");
			} else if(view.getF5().getText().equals("all")){
				int count = view.getModel2().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel2().removeRow(i);
				}
				employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Viewd all accounts.");
				view.populateAccountTable();
			} else {
				int count = view.getModel2().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel2().removeRow(i);
				}
				employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Viewd account number "+view.getF5().getText());
				view.populateAccount();
			}
	
			repository1 = new Repository(employeeWork);

			repository1.create(employeeWork);
			
		} else if (e.getSource() == view.getB7()) { // UPDATE ACCOUNT
			
			Calendar calendar = Calendar.getInstance();
     		java.util.Date currentDate = calendar.getTime();
			java.sql.Date date = new java.sql.Date(currentDate.getTime());
			
			if(view.getF5().getText().equals("") || view.getF6().getText().equals("") || view.getF3().getText().equals("") || view.getF7().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else {
				if(view.checkClient()){
					int count = view.getModel2().getRowCount();
					for(int i = count-1 ; i>=0; i--){
						view.getModel2().removeRow(i);
					}
					account = new Account(Integer.parseInt(view.getF5().getText()), view.getF6().getText(), Integer.parseInt(view.getF3().getText()), Integer.parseInt(view.getF7().getText()), date);
					employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Updated account number "+view.getF5().getText());

				} else {
					JOptionPane.showMessageDialog(null, "Client not found!");
				}
			}
					
			repository = new Repository(account);
			repository.update(account);
			
			repository1 = new Repository(employeeWork);
			repository1.update(employeeWork);
			
			view.populateAccountTable();
			
		} else if (e.getSource() == view.getB8()) { // DELETE ACCOUNT
			
			if(view.getF5().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete account ID!");
			} else{
				int count = view.getModel2().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel2().removeRow(i);
				}
				account = new Account(Integer.parseInt(view.getF5().getText()), null, 0, 0, null);
				employeeWork = new EmployeeWork(employeeGateway.findID(view.getF12().getText(), view.getF13().getText()), view.getF12().getText(), getDate(), "Deleted account number "+view.getF5().getText());

			}
			
			repository = new Repository(account);
			repository.delete(account);
			
			repository1 = new Repository(employeeWork);
			repository1.update(employeeWork);
			
			view.populateAccountTable();
			
		} else if (e.getSource() == view.getB9()) { // TRANSFER MONEY
			
			viewTransfer = new ViewTransfer();
			viewTransfer.getFrame().setVisible(true);
			
			
		} else if (e.getSource() == view.getB10()) { // PROCESS BILLS
			
		} else if (e.getSource() == view.getB11()) { // ADD EMPLOYEE
				
			if(view.getF8().getText().equals("") || view.getF9().getText().equals("") || view.getF10().getText().equals("") || view.getF11().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else{
				
				int count = view.getModel1().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel1().removeRow(i);
				}
				employee = new Employee(Integer.parseInt(view.getF8().getText()), view.getF9().getText(), Integer.parseInt(view.getF10().getText()), view.getF11().getText(), getDate());
				user = new User(view.getF9().getText(), "pass", view.getF6().getText(), Integer.parseInt(view.getF8().getText()));
				//public User(String userName, String password, String type, int employeeId)
			}
			
			repository = new Repository(employee);
			repository1 = new Repository(user);

			repository.create(employee);
			repository1.create(user);
			
			view.populateEmployeeTable();
			
		} else if (e.getSource() == view.getB12()) { // VIEW SPECIFIC EMPLOYEE
			
			
			if(view.getF8().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete employee ID!");
			} else{
				
				int count = view.getModel1().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel1().removeRow(i);
				}
				employee = new Employee(Integer.parseInt(view.getF8().getText()), null, 0, null, null);
			}
			
			repository = new Repository(employee);

			repository.read(employee);
			
			view.populateEmployee();

			
		} else if (e.getSource() == view.getB13()) { // UPDATE EMPLOYEE
			
			if(view.getF8().getText().equals("") || view.getF9().getText().equals("") || view.getF10().getText().equals("") || view.getF11().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informations!");
			} else{
				
				int count = view.getModel1().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel1().removeRow(i);
				}
				employee = new Employee(Integer.parseInt(view.getF8().getText()), view.getF9().getText(), Integer.parseInt(view.getF10().getText()), view.getF11().getText(), getDate());
				user = new User(view.getF9().getText(), "pass", view.getF6().getText(), Integer.parseInt(view.getF8().getText()));
			}
			
			repository = new Repository(employee);
			repository1 = new Repository(user);
			
			repository.update(employee);
			repository1.update(user);
			
			view.populateEmployeeTable();
			
		} else if (e.getSource() == view.getB14()) { // DELETE EMPLOYEE
			
			if(view.getF8().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete employee ID!");
			} else{
				
				int count = view.getModel1().getRowCount();
				for(int i = count-1 ; i>=0; i--){
					view.getModel1().removeRow(i);
				}
				employee = new Employee(Integer.parseInt(view.getF8().getText()), null, 0, null, null);
				user = new User(null, null, null, Integer.parseInt(view.getF8().getText()));
			}
			
			repository = new Repository(employee);
			repository1 = new Repository(user);

			repository.delete(employee);
			repository1.delete(user);
			
			view.populateEmployeeTable();
			
		} else if (e.getSource() == view.getB15()) { // EMPLOYEE WORK REPORT
			
			viewReport = new ViewReport();
			
			if(view.getF8().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Enter the employee ID!");
			} else {
				viewReport.populateWorkTable(view.getF8().getText());
			}
			
		} else if (e.getSource() == view.getB16()) { // VIEW ALL EMPLOYEES
			
			int count = view.getModel1().getRowCount();
			for(int i = count-1 ; i>=0; i--){
				view.getModel1().removeRow(i);
			}
			
			view.populateEmployeeTable();
		} else if (e.getSource() == view.getB17()) { // LOG OUT EMPLOYEE
			view.cleanP1();
			view.apearFirstP1();
			
			view.getF12().setText("");
			view.getF13().setText("");
			
		} else if (e.getSource() == view.getB18()) { // CHANGE PASSWORD EMPLOYEE
			viewPassword = new ViewPassword();
			viewPassword.getFrame().setVisible(true);
			
		} else if (e.getSource() == view.getB19()) { // LOG OUT ADMINISTRATOR 
			view.cleanP2();
			view.apearFirstP2();
			view.getF14().setText("");
			view.getF15().setText("");
			
		} else if (e.getSource() == view.getB20()) { // CHANGE PASSWORD ADMINISTRATOR
			
		} else if (e.getSource() == view.getB21()) { // LOGIN EMPLOYEE
			employeeGateway = new EmployeeGateway();
			
			if(view.getF12().getText().equals("") || view.getF13().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Complete all informtions!");
			} else {
				if(employeeGateway.checkUserPass(view.getF12().getText(), view.getF13().getText())){
					view.apearP1();
					view.cleanFirstP1();
					System.out.println(view.getF14().getText() + view.getF14().getText());
				} else {
					JOptionPane.showMessageDialog(null, "USER or PASSWORD is incorrect!");
				}
			}
			
			
		} else if (e.getSource() == view.getB22()) { // LOGIN ADMINISTRATOR
			
			if(view.getF14().getText().equals(userName) && view.getF15().getText().equals(password)){
				view.apearP2();
				view.cleanFirstP2();
			} else {
				JOptionPane.showMessageDialog(null, "USER or PASSWORD is incorrect!");
			}
			
		}    
		
	}
	
	public Date getDate(){
		Calendar calendar = Calendar.getInstance();
 		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());
		return date;
	}
}