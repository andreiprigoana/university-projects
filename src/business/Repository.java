package business;

import dataAccesLayer.AccountGateway;
import dataAccesLayer.ClientGateway;
import dataAccesLayer.EmployeeGateway;
import dataAccesLayer.EmployeeWorkGateway;
import dataAccesLayer.GenericDataGateway;
import dataAccesLayer.UserGateway;
import model.Account;
import model.Client;
import model.Employee;
import model.EmployeeWork;
import model.Entity;
import model.User;


public class Repository<TGateway extends GenericDataGateway, TEntity extends Entity> {
	
	
	private TGateway gateway;
	private TEntity entity;
	
	
	public Repository(TEntity entity){
		this.entity = entity;
		
		if(entity instanceof Client)
			this.gateway = (TGateway) new ClientGateway();
		if(entity instanceof Account)
		    this.gateway = (TGateway) new AccountGateway();
		if(entity instanceof Employee)
		    this.gateway = (TGateway) new EmployeeGateway();
		if(entity instanceof EmployeeWork)
		    this.gateway = (TGateway) new EmployeeWorkGateway();
		if(entity instanceof User)
		    this.gateway = (TGateway) new UserGateway();
	}
	
	public void create(TEntity entity){
		gateway.add(entity);
	}
	
	public void read(TEntity entity){
		gateway.view(entity);
	}
	
	public void update(TEntity entity){
		gateway.update(entity);
	}
	
	public void delete(TEntity entity){
		gateway.delete(entity);
	}

}