package dataAccesLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Account;

public class AccountGateway extends GenericDataGateway<Account>{
	
	private DBConnection connect;
	
	private static final String addAccount = "INSERT INTO account (accountID, type, CNP, money, date) VALUES (?, ?, ?, ?, ?)";
	private static final String updateAccount = "UPDATE account SET accountID=? , type=?, CNP=? , money=? , date=?  WHERE accountID=?";
	private static final String deleteAccount = "DELETE FROM account WHERE accountID=?";
	private static final String viewAccount = "SELECT FROM account WHERE CNP=?";
	private static final String transferMoney = "UPDATE account SET money=? WHERE accountID=?";

	@Override
	public void add(Account entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(addAccount);
	    	statement.setInt(1, entity.getAccountId());
	    	statement.setString(2, entity.getType());
	    	statement.setInt(3, entity.getCNP());
	    	statement.setInt(4, entity.getMoney());
	    	statement.setDate(5, entity.getDate());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	
	@Override
	public void update(Account entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(updateAccount);
	    	statement.setInt(1, entity.getAccountId());
	    	statement.setString(2, entity.getType());
	    	statement.setInt(3, entity.getCNP());
	    	statement.setInt(4, entity.getMoney());
	    	statement.setDate(5, entity.getDate());
	    	statement.setInt(6, entity.getAccountId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	

	@Override
	public void delete(Account entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(deleteAccount);
	    	statement.setInt(1, entity.getAccountId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	
	@Override
	public void view(Account entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(viewAccount);
	    	statement.setInt(1, entity.getCNP());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}


	@Override
	public void transferMoney(int account1, int account2, int money) { // ACCOUNT1 -> ACCOUNT2
		int oldMoneyAccount1 = 0;
		int oldMoneyAccount2 = 0;
		
		connect = new DBConnection();
		
		try{
			String querry1 = "SELECT * FROM account WHERE accountID="+account1;
			connect.setRs(connect.getSt().executeQuery(querry1));
			while(connect.getRs().next()){
				oldMoneyAccount1 = connect.getRs().getInt("money");
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
		
		try{
			String querry2 = "SELECT * FROM account WHERE accountID="+account2;
			connect.setRs(connect.getSt().executeQuery(querry2));
			while(connect.getRs().next()){
				oldMoneyAccount2 = connect.getRs().getInt("money");
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
				
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(transferMoney);
	    	statement.setInt(1, oldMoneyAccount1-money);
	    	statement.setInt(2, account1);
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(transferMoney);
	    	statement.setInt(1, oldMoneyAccount2+money);
	    	statement.setInt(2, account2);
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void processBills(Account entity) {
		// TODO Auto-generated method stub

	}

}
