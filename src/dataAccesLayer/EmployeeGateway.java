package dataAccesLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Employee;

public class EmployeeGateway extends GenericDataGateway<Employee>{
	
	private DBConnection connect;
	
	private static final String addEmployee = "INSERT INTO employee (employeeID, name, CNP, Address, date) VALUES (?, ?, ?, ?, ?)";
	private static final String updateEmployee = "UPDATE employee SET employeeID=? , name=? , CNP=? , Address=? , date=? WHERE employeeID=?";
	private static final String deleteEmployee = "DELETE FROM employee WHERE employeeID=?";
	private static final String viewEmployee = "SELECT * FROM employee WHERE employeeID=?";
	

	@Override
	public void add(Employee entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(addEmployee);
	    	statement.setInt(1, entity.getEmployeeId());
	    	statement.setString(2, entity.getName());
	    	statement.setLong(3, entity.getCNP());
	    	statement.setString(4, entity.getAddress());
	    	statement.setDate(5, entity.getDate());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	
	@Override
	public void update(Employee entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(updateEmployee);
	    	statement.setInt(1, entity.getEmployeeId());
	    	statement.setString(2, entity.getName());
	    	statement.setLong(3, entity.getCNP());
	    	statement.setString(4, entity.getAddress());
	    	statement.setDate(5, entity.getDate());
	    	statement.setInt(6, entity.getEmployeeId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	
	@Override
	public void delete(Employee entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(deleteEmployee);
	    	statement.setInt(1, entity.getEmployeeId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	
	@Override
	public void view(Employee entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(viewEmployee);
	    	statement.setInt(1, entity.getEmployeeId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	
	public boolean checkUserPass(String user, String password){
		String checkUser = null;
		String checkPassword = null;
		
		
		connect = new DBConnection();
		
		try{
			String querry1 = "SELECT * FROM user WHERE ( userName='"+user+"' AND password='"+password+"' )";
			connect.setRs(connect.getSt().executeQuery(querry1));
			while(connect.getRs().next()){
				checkUser = connect.getRs().getString("userName");
				checkPassword = connect.getRs().getString("password");
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
		
		if(checkUser.equals(user) && checkPassword.equals(password)){
			return true;
		} else if(checkUser==null || checkPassword==null){
			return false;
		} else {
			return false;
		}
	}
	
	
	public int findID(String user, String password){
		int userID = 0;
		
		connect = new DBConnection();
		
		try{
			String querry1 = "SELECT * FROM user WHERE ( userName='"+user+"' AND password='"+password+"' )";
			connect.setRs(connect.getSt().executeQuery(querry1));
			while(connect.getRs().next()){
				userID = connect.getRs().getInt("employeeID");
			}
		}catch(Exception ex){
			System.err.println(ex);
		}
		return userID;
	}
	

	@Override
	public void transferMoney(int account1, int account2, int money) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processBills(Employee entity) {
		// TODO Auto-generated method stub
		
	}
}
