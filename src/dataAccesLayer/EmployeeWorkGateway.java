package dataAccesLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.EmployeeWork;

public class EmployeeWorkGateway extends GenericDataGateway<EmployeeWork>{
	
	private DBConnection connect;
	
	private static final String addReport = "INSERT INTO employeework (employeeID, name, Date, Operation) VALUES (?, ?, ?, ?)";
	private static final String viewReport = "SELECT * FROM employeework WHERE employeeID =?";
	
	@Override
	public void add(EmployeeWork entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(addReport);
	    	statement.setInt(1, entity.getEmployeeId());
	    	statement.setString(2, entity.getName());
	    	statement.setDate(3, entity.getDate());
	    	statement.setString(4, entity.getOperation());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	
	@Override
	public void view(EmployeeWork entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(viewReport);
	    	statement.setInt(1, entity.getEmployeeId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	

	@Override
	public void update(EmployeeWork entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(EmployeeWork entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void transferMoney(int account1, int account2, int money) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processBills(EmployeeWork entity) {
		// TODO Auto-generated method stub
		
	}
}
