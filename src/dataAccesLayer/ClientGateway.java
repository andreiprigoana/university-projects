package dataAccesLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import model.Client;


public class ClientGateway extends GenericDataGateway<Client>{
	

	private DBConnection connect;
	
	private static final String addClient = "INSERT INTO client (name, cardID, CNP, Address) VALUES (?, ?, ?, ?)";
	private static final String updateClient = "UPDATE client SET name=? , cardID=? , CNP=? , Address=? WHERE CNP=?";
	private static final String viewClient = "SELECT FROM client WHERE CNP=?";
	
	
	@Override
	public void add(Client entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(addClient);
	    	statement.setString(1, entity.getName());
	    	statement.setInt(2, entity.getCardId());
	    	statement.setLong(3, entity.getCNP());
	    	statement.setString(4, entity.getAddress());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void update(Client entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(updateClient);
	    	statement.setString(1, entity.getName());
	    	statement.setInt(2, entity.getCardId());
	    	statement.setLong(3, entity.getCNP());
	    	statement.setString(4, entity.getAddress());
	    	statement.setLong(5, entity.getCNP());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	
	@Override
	public void view(Client entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(viewClient);
	    	statement.setInt(1, entity.getCNP());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	public void delete(Client entity) {
		// TODO Auto-generated method stub
	}

	@Override
	public void transferMoney(int account1, int account2, int money) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processBills(Client entity) {
		// TODO Auto-generated method stub
		
	}
}
