package dataAccesLayer;

import model.Entity;

public abstract class GenericDataGateway<TEntity extends Entity>{
	
	public abstract void add(TEntity entity);
	
	public abstract void update(TEntity entity);
	
	public abstract void delete(TEntity entity);
	
	public abstract void view(TEntity entity);
	
	public abstract void transferMoney(int account1, int account2, int money);
	
	public abstract void processBills(TEntity entity);
}