package dataAccesLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.User;




public class UserGateway extends GenericDataGateway<User>{
	
	private DBConnection connect;
	
	private static final String addPassUser = "INSERT INTO user (password, type, userName, employeeID) VALUES (?, ?, ?, ?)";
	private static final String updatePass = "UPDATE user SET password=? WHERE employeeID=?";
	private static final String deletePassUser = "DELETE FROM user WHERE employeeID=?";

	@Override
	public void add(User entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(addPassUser);
	    	statement.setString(1, entity.getPassword());
	    	statement.setString(2, entity.getType());
	    	statement.setString(3, entity.getUserName());
	    	statement.setInt(4, entity.getEmployeeId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void update(User entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(updatePass);
	    	statement.setString(1, entity.getPassword());
	    	statement.setInt(2, entity.getEmployeeId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void delete(User entity) {
		connect = new DBConnection();
		
		try {
			Connection con = connect.getCon();
	    	
	    	PreparedStatement statement = con.prepareStatement(deletePassUser);
	    	statement.setInt(1, entity.getEmployeeId());
	    	statement.executeUpdate();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	

	@Override
	public void view(User entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void transferMoney(int account1, int account2, int money) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processBills(User entity) {
		// TODO Auto-generated method stub
	
	}
}
